import React, { Component } from 'react';

class Item extends Component {

  constructor(props) {
    super(props);
    //props.item
    //props.clickEdit
    //props.clickDelete

    this.onClickDelete = this.onClickDelete.bind(this);

  }

  onClickDelete(id) {
    this.props.clickDelete(id);
  }

  onClickEdit(item) {
    this.props.clickEdit(item);
  }

  render() {
    const { item } = this.props;
    const { index } = this.props;

    let elmBadges = null;
    if (item.level === 0) {
      elmBadges = <span className="badge bg-secondary">Small</span>;
    }
    else if (item.level === 1) {
      elmBadges = <span className="badge bg-info text-dark">Medium</span>;
    }
    else {
      elmBadges = <span className="badge bg-danger">High</span>
    }

    return (
      <tr>
        <th>{index + 1}</th>
        <td>{item.name}</td>
        <td className="icon-badges">{elmBadges}</td>
        <td>
          <button onClick={() => this.onClickDelete(item.id)} type="button" className="btn btn-danger">Delete</button>
          <button onClick={() => this.onClickEdit(item)} type="button" className="btn btn-warning">Edit</button>
        </td>
      </tr>
    );
  }
}

export default Item;
