import React, { Component } from 'react';

class Search extends Component {
    render() {
        return (
            <div className="search input-group mb-3">
                <input type="text" className="form-control" placeholder="Tìm kiếm"></input>
                <button className="btn btn-success" type="button">Search</button>
                <button className="btn btn-warning" type="button">Clear</button>
            </div>
        );
    }
}

export default Search;
