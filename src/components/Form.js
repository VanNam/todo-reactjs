import React, { Component } from 'react';

class Form extends Component {

    constructor(props) {
        super(props);
        //props.clickAddTodo
        //props.isShowForm
        //props.ClickisShow
        //props.itemSelected

        this.state = {
            isShowForm: this.props.isShowForm,
            task_id: '',
            task_name: '',
            task_level: 0
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCancle = this.handleCancle.bind(this);
    }

    UNSAFE_componentWillMount (){
        let {itemSelected} = this.props;
        if(itemSelected !== null) {
            this.setState({
                task_id: itemSelected.id,
                task_name: itemSelected.name,
                task_level: itemSelected.level
            })
        }

    }

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {
        let itemSelected = nextProps.itemSelected;
        if(nextProps !== null) {
            this.setState({
                task_id: itemSelected.id,
                task_name: itemSelected.name,
                task_level: itemSelected.level
            })
        }
    }

    handleSubmit(event) {
        let item = {
            id: this.state.task_id,
            name: this.state.task_name,
            level: +this.state.task_level
        }
        this.props.clickAddTodo(item);
        this.props.ClickisShow();
        
        event.preventDefault();
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleCancle() {
        this.props.ClickisShow();
    }

    render() {
        return (
            <div className="form">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" className="form-name form-control" value={this.state.task_name}
                        name="task_name"
                        onChange={this.handleChange} ></input>
                    <select onChange={this.handleChange} value={this.state.task_level} className="form-select select" name="task_level">
                        <option value={0}>Small</option>
                        <option value={1}>Medium</option>
                        <option value={2}>High</option>
                    </select>
                    <button type="submit" className="btn btn-success btn-submit">Submit</button>
                    <button onClick={this.handleCancle} type="button" className="btn btn-info">Cancle</button>
                </form>
            </div>
        );
    }
}

export default Form;
