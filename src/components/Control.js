import React, { Component } from 'react';
import Search from './Search';

class Control extends Component {

  constructor(props) {
    super(props);
    //props.clickisShow
    //props.isShowForm

    this.state = {

    }

    this.onShowForm = this.onShowForm.bind(this);

  }

  onShowForm() {
    this.props.clickisShow();
  }

  render() {
    let elmChangeForm = <button onClick={this.onShowForm} type="button" className="btn btn-primary btn-add">Add Todo</button>
    
    if(this.props.isShowForm === true) {
      elmChangeForm = <button onClick={this.onShowForm} type="button" className="btn btn-success btn-add">Close Form</button>
    }

    return (
      <div className="control">
        <Search />
        {elmChangeForm}
      </div>
    );
  }
}

export default Control;
