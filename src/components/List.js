import React, { Component } from 'react';
import Item from './Item';

class List extends Component {

    constructor(props) {
        super(props);
        //props.items
        //props.clickEdit
        //props.clickDelete
    }

    render() {

        const { items } = this.props;
        const elmItem = items.map((item, index) => {
            return (
                <Item item={item} key={index} index={index} clickDelete={this.props.clickDelete} clickEdit={this.props.clickEdit}/>
            );
        })

        return (
            <div className="list">
                <table className="table table-hover table-text">
                    <thead>
                        <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Task</th>
                            <th scope="col">Level</th>
                            <th scope="col">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {elmItem}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default List;
