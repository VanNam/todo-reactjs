import React, { Component } from 'react';
import './App.css';
import _ from 'lodash';
//
import Title from './components/Title';
import Control from './components/Control';
import List from './components/List';
import Form from './components/Form';
import task from './mock/task';

const { v4: uuidv4 } = require('uuid');

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isShowForm: false,
      items: task,
      itemSelected: null
    }

    this.ClickisShow = this.ClickisShow.bind(this);
    this.clickDelete = this.clickDelete.bind(this);
    this.clickEdit = this.clickEdit.bind(this);
    this.clickAddTodo = this.clickAddTodo.bind(this);
  }

  ClickisShow() {
    this.setState({
      isShowForm: !this.state.isShowForm
    });
  }

  clickAddTodo(item) {
    let { items } = this.state;
    if (item.id !== '') {
      console.log('item', item);
      items = _.reject(items, { id: item.id });
      items.push({
        id: item.id,
        name: item.name,
        level: item.level
      })
    } else {
      items.push({
        id: uuidv4(),
        name: item.name,
        level: item.level
      })
    }

    this.setState({
      items: items,
      itemSelected: null
    });

  }

  clickEdit(item) {
    this.setState({
      itemSelected: item,
      isShowForm: true
    });
  }

  clickDelete(id) {
    let items = this.state.items;
    _.remove(items, (item) => {
      return item.id === id;
    });
    this.setState({
      items: items
    });
  }

  render() {
    let { isShowForm, items, itemSelected } = this.state;
    let elmShowForm = null;

    if (isShowForm === true) {
      elmShowForm = <Form clickAddTodo={this.clickAddTodo}
        isShowForm={isShowForm}
        ClickisShow={this.ClickisShow}
        itemSelected={itemSelected} />
    }
    else {
      elmShowForm = null;
    }

    return (
      <div className="App">
        <Title />
        <Control clickisShow={this.ClickisShow} isShowForm={isShowForm} />
        {elmShowForm}
        <List items={items} clickDelete={this.clickDelete} clickEdit={this.clickEdit} />
      </div>
    );
  }
}

export default App;
