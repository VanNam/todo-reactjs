const { v4: uuidv4 } = require('uuid');

let task = [
    {
      id: uuidv4(),
      name: 'abc',
      level: 0
    },
    {
      id: uuidv4(),
      name: 'abc21',
      level: 1
    },
    {
      id: uuidv4(),
      name: 'abcggg',
      level: 1
    },{
      id: uuidv4(),
      name: 'abc55555555',
      level: 2
    }
  ];

  export default task;